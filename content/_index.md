---
title: HOME
description: Welcome to MOOD Events
#images: ["/images/sample.jpg"]
---

Hey,

I'm Robert Arkenklo, Light and Stage technician in Malmö. 

I'm especially good at nightclub and rave moods. I've done lights at places like Plan B, Inkonst and some more underground local stuff.

The plan was to do light technician work fulltime, but 2020 had other plans :).
For 2021 I'm available nights and weekends.\
Please email me at gigs@arkenklo.se.

[Check out my Portfolio](/portfolio "Check out my Portfolio")

[Or look at my electronics projects on the  blog](https://blog.arkenklo.se/ "Or look at my electronics projects on the blog")
